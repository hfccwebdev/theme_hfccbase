<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?><!DOCTYPE html>
<html lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body class="<?php print $classes; ?>">
  <div id="page">
    <!-- HEADER -->
    <header id="section-header" class="clearfix">
      <?php if (!empty($logo)): ?>
        <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
          <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
          <?php if (!empty($header)): ?>
            <div class="region region-header">
              <?php print $header; ?>
            </div>
          <?php endif; ?>
        </div> <!-- /name-and-slogan -->
      <?php endif; ?>
    </header>
    <!-- MAIN -->
    <div id="section-main" class="column"><div id="main-inner">
      <div id="content" class="column"></div id="content-inner" class="clearfix">
        <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php if (!empty($messages)): print $messages; endif; ?>
        <div class="region region-content">
          <?php print $content; ?>
        </div> <!-- /region-content -->
      </div> <!-- /content -->
      <!-- SIDEBAR FIRST -->
      <?php if (!empty($sidebar_first)): ?>
        <aside id="sidebar-first" class="column sidebar">
          <?php print $sidebar_first; ?>
        </aside> <!-- /#sidebar-first -->
      <?php endif; ?>
      <!-- SIDEBAR SECOND -->
      <?php if (!empty($sidebar_second)): ?>
        <aside id="sidebar-second" class="column sidebar">
          <?php print $sidebar_second; ?>
        </aside> <!-- /#sidebar-second -->
      <?php endif; ?>
    </div></div> <!-- /#main-inner, /#section-main -->
    <!-- FOOTER -->
    <footer id="section-footer" class="clearfix">
      <?php if (!empty($footer)): print $footer; endif; ?>
    </footer>
  </div> <!-- /#page -->
</body>
</html>
